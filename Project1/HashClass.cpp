
#include "HashClass.h"
#define TABLE_SIZE 300
//Costruttore Alloco la tabella Hash ed imposto le voci a Null
HashClass::HashClass(){
    this->hashTable=new HashItem*[TABLE_SIZE];
    for (int i=0; i< TABLE_SIZE; i++) {
        this->hashTable[i]=NULL;
            
    }
}
//Distruttore libero la memoria
HashClass::~HashClass(){
    for (int i=0 ; i<TABLE_SIZE; i++) {
        //Controllo se la posizione i della tabella è vuota
        if (this->hashTable[i]!=NULL) {
            HashItem *ptrPrev = NULL;
            HashItem *entry = this->hashTable[i];
            while (entry!=NULL) {
                ptrPrev=entry;
                entry=entry->getNext();
                delete ptrPrev;
            }
        }
    }
    delete [] this->hashTable;
}


//Analisi delle prestazioni
void HashClass::ricercaConSuccesso(){
    float alpha=0.0;
    float n =0.0;
    /*Calcolo gli slot occupati facciò ciò poichè mi serve per calcolarmi
    il fattore di carico
     */
    for(int i=0;i<TABLE_SIZE;i++){
        if (this->hashTable[i]!=NULL) {
            n++;
        }
    }
    //Fattore di carico = numero di elementi totali/slot
    alpha=float((n/TABLE_SIZE));
    //Ricerca con successo 1+alpha
    float mediRicerca = 1+alpha;
    std::cout<<"Il tempo medio della ricerca con successo è: "<<mediRicerca<<" sec\n";
}

/*Cerca nel vocabolario
il metodo prende in input il termine da cercare*/
HashItem* HashClass::cerca(std::string termine){
    int index = hashMethod(termine);//Calcolo il valore hash
    //Non trovo la parola
    if (this->hashTable[index]==NULL) {
        return NULL;
    }else{
        HashItem *entry = this->hashTable[index];
        //Scorro l'intera lista
        while (entry!=NULL && entry->getHashVal()!= index) {
            entry=entry->getNext();
        }
        //Se non trovo la parola ritorno NULL
        if (entry==NULL) {
            return NULL;
        }else{
            return entry;
        }
    }
}



void HashClass::cercaAlternativa(HashItem *termine){
    int lcs;
    int  max =0;
    std::list <HashItem> parola;
    
    //In questo ciclo trovo tutte le parole con la max sottosequenza
    for (int i=0; i< TABLE_SIZE; i++) {
        if (this->hashTable[i]!=NULL) {
            lcs = lcsLength(termine->getTermine(), hashTable[i]->getTermine());
            if (max<lcs) {
                max=lcs;
            }
        }
    }
    
    //Aggiungo tutte le parola con la max sotto sequenza in una lista
    for (int i=0; i<TABLE_SIZE; i++) {
        if (this->hashTable[i]!=NULL) {
            lcs = lcsLength(termine->getTermine(), hashTable[i]->getTermine());
            /*Verifico se il valore dell lcs è uguale al valore max calcolato
            nel ciclo precedente se è così aggiungo il termine alla mia 
             lista di possibili parole */
            if (lcs == max) {
                parola.push_back(*hashTable[i]);
            }
        }
    }
    //Stampo tutti i possibili termi che l'utete forse cercava
    std::cout<<"\nForse cercavi:\n";
    /*Utilizzo un iteratore per scorrere la lista esso non è nient'altro che un puntatore a un contenitore
     In questo caso il nostro contenitore è una lista */
    
    std::list<HashItem>::iterator iterator =parola.begin();
    while (iterator!=parola.end() ){
        std::cout<<iterator->getTermine()<<" : "<<iterator->getSignificato()<<"\n";
        iterator++;
    }
}


// Restituisce la lunghezza dell LCS per le stringhe
int HashClass:: lcsLength(std::string X,std:: string Y)
{
    int m = int (X.length()+1);
    int n = int(Y.length()+1);
    int i, j;
    int L[m+1][n+1];
    for (i=0; i<=m; i++)
    {
        for (j=0; j<=n; j++)
        {
            //Per i =0 o j =0 lcs è 0
            if (i == 0 || j == 0){
                L[i][j] = 0;
            }
            //i due elementi sono uguali e quindi inserisco quell'elemento nell' lvs
            else if (X[i-1] == Y[j-1]){
                L[i][j] = L[i-1][j-1] + 1;
            }
            //I due elementi sono diversi quindi calcolco lcs (i-1,j)e(i,j-1)
            //per poi considerare la più lunga di queste due come LCS di X e Y
            else{
                L[i][j] = std::max(L[i-1][j], L[i][j-1]);
            }
        }
    }
    
    return L[m][n];
}

void HashClass:: riempi(){
    this->inserisci("casa", "abitazione");
    this->inserisci("computer", "apparecchio informatico");
    this->inserisci("stanza", "parte di una casa");
    this->inserisci("televisore", "Apparecchio visivo");
    this->inserisci("scatola", "contenitore");
    this->inserisci("cane", "animale");
    this->inserisci("sciarpa", "indumento");
    this->inserisci("iphone", "cellulare");
    this->inserisci("android", "sistema operativa");
    this->inserisci("spark fun", "negozio online");
    this->inserisci("arduino", "scheda elettronica");
    this->inserisci("google", "motore di ricerca");
    this->inserisci("italia uno", "canale televisivo");
    this->inserisci("hard disk", "periferica");
    this->inserisci("mouse", "periferica");
    this->inserisci("play station", "console");
    this->inserisci("wii", "console");
    this->inserisci("xbox", "console");
    this->inserisci("lettore cd", "periferica");
    this->inserisci("spotify", "software");
    this->inserisci("java", "linguaggio di programmazione");
    this->inserisci("gatto", "animale");
    this->inserisci("scimmia", "animale");
    this->inserisci("pantalone", "vestito");
    this->inserisci("mela", "frutta");
    this->inserisci("pera", "frutta");
    this->inserisci("banana", "frutta");
    this->inserisci("fragola", "frutta");
    this->inserisci("arancia", "frutta");
    this->inserisci("ananas", "frutta");
    this->inserisci("lampone", "frutta");
    this->inserisci("melone", "frutta");
    this->inserisci("xcode", "software");
    this->inserisci("netbeans", "software");
    this->inserisci("firefox", "browser");
    this->inserisci("fantozzi", "film");
    this->inserisci("dracula", "film");
    this->inserisci("conan", "film");
    this->inserisci("proxifier", "software");
    this->inserisci("ibook", "software");
    this->inserisci("panda", "animale");
    this->inserisci("stereo", "periferica");
    this->inserisci("pesce", "animale");
    this->inserisci("mucca", "animale");
    this->inserisci("scimmia", "animale");
    this->inserisci("cammello", "animale");
    this->inserisci("elefante", "animale");
    this->inserisci("mosca", "animale");
    this->inserisci("antilope", "animale");
    this->inserisci("bradipo", "animale");
    this->inserisci("coccodrillo", "animale");
    this->inserisci("dinosauro", "animale");
    this->inserisci("farfalla", "insetto");
    this->inserisci("giraffa", "animale");
    this->inserisci("lumaca", "insetto");
    this->inserisci("papera", "animale");
    this->inserisci("rana", "animale");
    this->inserisci("sacco", "oggetto");
    this->inserisci("stivale", "vestiti");
    this->inserisci("italia", "nazione");
    this->inserisci("germania", "nazione");
    this->inserisci("austria", "nazione");
    this->inserisci("francia", "nazione");
    this->inserisci("inghilterra", "nazione");
    this->inserisci("svizzera", "nazione");
    this->inserisci("danimarca", "nazione");
    this->inserisci("olanda", "nazione");
    this->inserisci("romania", "nazione");
    this->inserisci("argentina", "nazione");
    this->inserisci("peru", "nazione");
    this->inserisci("brasile", "nazione");
    this->inserisci("cuba", "nazione");
    this->inserisci("giappone", "nazione");
    this->inserisci("cina", "nazione");
    this->inserisci("canada", "nazione");
    this->inserisci("bucarest", "citta");
    this->inserisci("budapest", "citta");
    this->inserisci("napoli", "citta");
    this->inserisci("milano", "citta");
    this->inserisci("roma", "citta");
    this->inserisci("firenze", "citta");
    this->inserisci("torino", "citta");
    this->inserisci("londra", "citta");
    this->inserisci("berlino", "citta");
    this->inserisci("dublino", "citta");
    this->inserisci("parigi", "citta");
    this->inserisci("mantova", "citta");
    this->inserisci("venezia", "citta");
    this->inserisci("firenze", "citta");
    this->inserisci("palermo", "citta");
    this->inserisci("avellino", "citta");
    this->inserisci("bologna", "citta");
    this->inserisci("rimini", "citta");
    this->inserisci("riccione", "citta");
    this->inserisci("amsterdam", "citta");
    this->inserisci("sorrento", "citta");
    this->inserisci("grecia", "nazione");
    this->inserisci("calcio", "sport");
    this->inserisci("tennis", "sport");
    this->inserisci("sci", "sport");
    this->inserisci("basket", "sport");
    this->inserisci("golf", "sport");
    this->inserisci("box", "sport");
    this->inserisci("ginnastica", "sport");
    this->inserisci("karate", "sport");
    this->inserisci("casa", "abitazione");
}



void HashClass::inserisci(std::string termine, std::string significato){
    //Calcolo l'hash della parola tramite il metodo hashMethod
    int index= hashMethod(termine);
    //effettuo un controllo per verificare che la posizione dove si vuole inserire il termine sia libera
    if (this->hashTable[index]==NULL){
        this->hashTable[index]= new HashItem(index,termine,significato);
    }
    else{
        //Scorro la lista fino a trovare uno slot libero dove inserire il nuovo termine
        HashItem * entry = this->hashTable[index];
        while (entry->getNext()!=NULL) {
            entry=entry->getNext();
            if (entry->getHashVal()==index) {
                entry->setTermine(termine);
            }else{
                entry->setNext(new HashItem(index, termine , significato ));
            }
        }
    }
}

void HashClass::rimuovi(std::string termine){
    //Ottengo la posizione dov'è memorizzato il termine da cancellare
    int index= hashMethod(termine);
    
    if (this->hashTable[index]!=NULL) {
        HashItem *ptrPrev = NULL;
        HashItem *ptr= this->hashTable[index];
        //scorro la lista fino a trovare l'elemento da cancellare
        while (ptr->getNext()!=NULL && ptr->getHashVal()!= index) {
            ptrPrev=ptr;
            ptr=ptr->getNext();
        }
        //Ho trovato l'lelemento
        if (ptr->getHashVal()==index) {
            HashItem *nextPtr= ptr->getNext();
            delete ptr;
            this->hashTable[index]=nextPtr;
        }else{
            //Avanzo
            HashItem *next =  ptr->getNext();
            delete ptr;
            ptrPrev->setNext(next);
            
        }
    }
}

//Visualizzo il vocabolario
void HashClass::stampaVocabolario(){
    std::cout << "\nContenuto del vocabolario:\n\n";
    for (int i=0; i< TABLE_SIZE; i++) {
        if(this->hashTable[i]!=NULL){
            std::cout<<this->hashTable[i]->getTermine()<<"->"<<this->hashTable[i]->getSignificato()<<"\n";
        }
    }
}


/* CALCOLO FUNZIONE HASH (METODO DELLA DIVISIONE)
 Il valore  di una parola  e' calcolato interpretando  come un numero in base 27
 dove la cifra 'a' vale 1, 'b' vale 2, 'c' vale 3, ... , 'z' vale 26. 
*/
int HashClass::hashMethod(std::string parolaChiave) {
    int index =0;
    for (int i = 0 ;i<parolaChiave.length();i++){
        index=(27*index + parolaChiave[i])%TABLE_SIZE;
    }
    return index;
}