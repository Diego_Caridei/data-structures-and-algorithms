
#include <iostream>
#include <list>
#include "HashItem.h"
class HashClass {
private:
   //Tabella Hash
   HashItem**  hashTable;
public:
    HashClass();
    ~HashClass();
    HashItem *cerca(std::string termine);
    int  hashMethod(std::string parolaChiave);
    void rimuovi(std::string termine);
    void inserisci(std::string termine, std::string significato);
    void stampaVocabolario();
    void cercaAlternativa (HashItem *termine);
    int lcsLength(std::string X,std:: string Y);
    void ricercaConSuccesso();
    void riempi();
};