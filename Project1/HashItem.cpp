#include "HashItem.h"
//Costruttore
HashItem:: HashItem (int hashVal, std::string termine, std::string significato){
    this->hashVal =hashVal;
    this->termine = termine;
    this->significato = significato;
    this ->next = NULL;
}
//Distruttore
HashItem :: ~HashItem(){
    
}
//Setter
void HashItem:: setTermine(std::string termine){
    this->termine=termine;
}
void HashItem:: setSignificato(std::string significato){
    this->significato = significato;
}
void HashItem:: setNext(HashItem *next){
    this->next = next;
}
void HashItem::setHashVal(int hashVal){
    this->hashVal=hashVal;
}
//Getter
std::string HashItem:: getTermine(){
    return this->termine;
}
std::string HashItem:: getSignificato(){
    return this-> significato;
}
HashItem* HashItem::getNext(){
    return  this->next;
}
int HashItem:: getHashVal(){
    return this->hashVal;
}

