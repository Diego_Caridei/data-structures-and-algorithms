//  main.cpp
//  Progetto1
//
//  Created by Diego Caridei on 09/01/15.
//  Copyright (c) 2015 Diego Caridei. All rights reserved.
//

#include "HashClass.h"
int main(int argc, const char * argv[]) {
    
    std::cout<<"***********************************************\n";
    std::cout<<"* Progetto algoritmi e strutture dati         *\n";
    std::cout<<"* Hash Table: metodo della concatenazione     *\n";
    std::cout<<"* Diego Caridei Matricola 0124000311          *\n";
    std::cout<<"***********************************************\n";
    
    HashClass *dizionario = new HashClass();
    int scelta=0;
    std::string termine,significato;
    
    while (1) {
        std::cout<<"\n";
        std::cout<<"1) Aggiungi un nuovo  termine nel vocabolario\n";
        std::cout<<"2) Cerca un termine\n";
        std::cout<<"3) Cancella un termine\n";
        std::cout<<"4) Stampa il vocabolario\n";
        std::cout<<"5) Riempi il vocabolario con i termini di default\n";
        std::cout<<"6) Per uscire dal programma\n";
        std::cin>>scelta;
        
        //Verifico l' input nel caso inserissi un carattere invece che un intero
        if (std::cin.fail()) {
            std::cin.clear();
            std::cin.ignore();
            std::cout<<"\nComando1 non valido\n";
            continue;
        }
        else if (scelta == 6){
            std::cout<<"Arrivederci\n";
            break;
        }
        std:: cin.ignore();
        
        
        switch (scelta) {
                //Inserisco un nuovo termine nel vocabolario
            case 1:{
                std::cout<<"Inserisci il nuovo termine: ";
                getline(std::cin, termine);
                std::cout<<"Inserisci relativo significato del termine: ";
                getline(std::cin, significato);
                dizionario->inserisci(termine,significato);
                termine.clear();
                significato.clear();
                break;
            }
                //Ricerca del termine
            case 2:{
                
                std::cout<<"Inserisci il termine da cercare: ";
                getline(std::cin, termine);
                HashItem *termineDaCercare=dizionario->cerca(termine);
                if (termineDaCercare!=NULL) {
                    std::cout<<"Parola trovata\n";
                    std::cout<< termineDaCercare->getTermine()<<" --> "<<termineDaCercare->getSignificato()<<"\n";
                    dizionario->ricercaConSuccesso();
                }
                else{
                    HashItem *parola = new HashItem (dizionario->hashMethod(termine),termine,"");
                    dizionario->cercaAlternativa(parola);
                }
                termine.clear();
                significato.clear();
                break;
            }
                //Cancella un termine
            case 3:{
                std::cout<<"Inserisci il termine da cancellare: ";
                getline(std::cin, termine);
                dizionario->rimuovi(termine);
                break;
            }
            case 4:
                dizionario->stampaVocabolario();
                break;
            case 5:
                dizionario->riempi();
                break;
            case 6:
                std::cout<<"Arrivederci\n";
                break;
            default:
                std::cout<<"Scelta non valida\n";
                break;
        }
    }
    
    return 0;
}
