#include "HashClass.h"
#define TABLE_SIZE  300
//Costruttore alloca la memoria
HashClass::HashClass(){
    this->size=0;
    hashTable = new HashItem*[TABLE_SIZE];
    for (int i = 0; i < TABLE_SIZE; i++){
        hashTable[i] = NULL;
    }
    this->numeroPrimo =getPrime();
}
//Distruttore libero la memoria
HashClass::~HashClass() {
    for (int i = 0; i < TABLE_SIZE; i++){
        if (this->hashTable[i] != NULL) {
            delete hashTable[i];
            
        }
    }
    delete[] hashTable;
}
//Ritorna il nummero delle chiavi
int HashClass ::getSize(){
    return size;
}

HashItem* HashClass:: cerca(std::string termine){
      int hash1 = hashMethod (termine);
      return hashTable[hash1];
}


/*Per cancellare un termine dal dizionario non si può
 semplicemente marcare quello slot come vuoto (inserendo Nil),
 perché così facendo spezzeremo la sequenza di scansione di una chiave K inserita
 precedentemente, che ha saltato quello slot perché occupato.
 La soluzione utilizzata per risolvere questa problematica
 è stata  quella di marcare lo slot come DELETED
 e adattare l'algoritmo d'insrimento
 */

void HashClass::rimuovi(std::string termine){
    int hash = hashMethod(termine);
    
    if (hashTable[hash] == NULL ){
        std::cout<<"Il termine che si desidera cancellare non è presente\n";
    }
    else{
        std::cout<<"il termine è stato cancellato\n";
        hashTable[hash]->setTermine("DELETED");
        hashTable[hash]->setSignificato("");
        size--;
    }
}

void HashClass::inserisci(std::string termine,std::string significato){
    //Verifico se c'è ancora spazio nella tabella
    if (getSize()==TABLE_SIZE) {
        std::cout<<"Tabella piena non è possibile inserire altri elementi";
        return;
    }
    int hash1 = hashMethodUno(termine);
    int hash2= hashMethodDue(termine);
    while (hashTable[hash1]!=NULL) {
        
        if (hashTable[hash1]==NULL || hashTable[hash1]->getTermine().compare("DELETED")) {
            hash1 += hash2;
            hash1 %= TABLE_SIZE;
        }
        break;
    }
    hashTable[hash1]=new HashItem(hash1,termine,significato);
    size++;
}





//Calcola l hash doppio  (h1(k) + ih2(k)) mod m
int HashClass:: hashMethod(std:: string x){
    int hash1 = hashMethodUno(x);
    int hash2= hashMethodDue(x);
    while (hashTable[hash1]!=NULL && hashTable[hash1]->getTermine().compare(x)) {
        hash1 += hash2;
        hash1 %= TABLE_SIZE;
    }
    return hash1;
}



void HashClass:: riempi(){
    this->inserisci("computer", "apparecchio informatico");
    this->inserisci("stanza", "parte di una casa");
    this->inserisci("televisore", "Apparecchio visivo");
    this->inserisci("scatola", "contenitore");
    this->inserisci("cane", "animale");
    this->inserisci("sciarpa", "indumento");
    this->inserisci("iphone", "cellulare");
    this->inserisci("android", "sistema operativa");
    this->inserisci("spark fun", "negozio online");
    this->inserisci("arduino", "scheda elettronica");
    this->inserisci("google", "motore di ricerca");
    this->inserisci("italia uno", "canale televisivo");
    this->inserisci("hard disk", "periferica");
    this->inserisci("mouse", "periferica");
    this->inserisci("play station", "console");
    this->inserisci("wii", "console");
    this->inserisci("xbox", "console");
    this->inserisci("lettore cd", "periferica");
    this->inserisci("spotify", "software");
    this->inserisci("java", "linguaggio di programmazione");
    this->inserisci("gatto", "animale");
    this->inserisci("scimmia", "animale");
    this->inserisci("pantalone", "vestito");
    this->inserisci("mela", "frutta");
    this->inserisci("pera", "frutta");
    this->inserisci("banana", "frutta");
    this->inserisci("fragola", "frutta");
    this->inserisci("arancia", "frutta");
    this->inserisci("ananas", "frutta");
    this->inserisci("lampone", "frutta");
    this->inserisci("melone", "frutta");
    this->inserisci("xcode", "software");
    this->inserisci("netbeans", "software");
    this->inserisci("firefox", "browser");
    this->inserisci("fantozzi", "film");
    this->inserisci("dracula", "film");
    this->inserisci("conan", "film");
    this->inserisci("proxifier", "software");
    this->inserisci("ibook", "software");
    this->inserisci("panda", "animale");
    this->inserisci("stereo", "periferica");
    this->inserisci("pesce", "animale");
    this->inserisci("mucca", "animale");
    this->inserisci("scimmia", "animale");
    this->inserisci("cammello", "animale");
    this->inserisci("elefante", "animale");
    this->inserisci("mosca", "animale");
    this->inserisci("antilope", "animale");
    this->inserisci("bradipo", "animale");
    this->inserisci("coccodrillo", "animale");
    this->inserisci("dinosauro", "animale");
    this->inserisci("farfalla", "insetto");
    this->inserisci("giraffa", "animale");
    this->inserisci("lumaca", "insetto");
    this->inserisci("papera", "animale");
    this->inserisci("rana", "animale");
    this->inserisci("sacco", "oggetto");
    this->inserisci("stivale", "vestiti");
    this->inserisci("italia", "nazione");
    this->inserisci("germania", "nazione");
    this->inserisci("austria", "nazione");
    this->inserisci("francia", "nazione");
    this->inserisci("inghilterra", "nazione");
    this->inserisci("svizzera", "nazione");
    this->inserisci("danimarca", "nazione");
    this->inserisci("olanda", "nazione");
    this->inserisci("romania", "nazione");
    this->inserisci("argentina", "nazione");
    this->inserisci("peru", "nazione");
    this->inserisci("brasile", "nazione");
    this->inserisci("cuba", "nazione");
    this->inserisci("giappone", "nazione");
    this->inserisci("cina", "nazione");
    this->inserisci("canada", "nazione");
    this->inserisci("bucarest", "citta");
    this->inserisci("budapest", "citta");
    this->inserisci("napoli", "citta");
    this->inserisci("milano", "citta");
    this->inserisci("roma", "citta");
    this->inserisci("firenze", "citta");
    this->inserisci("torino", "citta");
    this->inserisci("londra", "citta");
    this->inserisci("berlino", "citta");
    this->inserisci("dublino", "citta");
    this->inserisci("parigi", "citta");
    this->inserisci("mantova", "citta");
    this->inserisci("venezia", "citta");
    this->inserisci("firenze", "citta");
    this->inserisci("palermo", "citta");
    this->inserisci("avellino", "citta");
    this->inserisci("bologna", "citta");
    this->inserisci("rimini", "citta");
    this->inserisci("riccione", "citta");
    this->inserisci("amsterdam", "citta");
    this->inserisci("sorrento", "citta");
    this->inserisci("grecia", "nazione");
    this->inserisci("calcio", "sport");
    this->inserisci("tennis", "sport");
    this->inserisci("sci", "sport");
    this->inserisci("basket", "sport");
    this->inserisci("golf", "sport");
    this->inserisci("box", "sport");
    this->inserisci("ginnastica", "sport");
    this->inserisci("karate", "sport");
    this->inserisci("casa", "abitazione");
}







/* CALCOLO FUNZIONE HASH 1 
 h1(k)=k mod m
 Il valore  di una parola  e' calcolato interpretando  come un numero in base 27
 dove la cifra 'a' vale 1, 'b' vale 2, 'c' vale 3, ... , 'z' vale 26.
 */
int HashClass:: hashMethodUno(std::string x){
    int index =0;
    for (int i = 0 ;i<x.length();i++){
        index=(index + x[i])%TABLE_SIZE;
    }
    
    return index;
}

/*
 E` necessario che h2(k) ed m siano primi tra loro poichè 
 se hanno un divisore in comune si generano ciclicamente sempre
 le stesse posizioni.
 */
int HashClass:: hashMethodDue (std::string x){
    int hashVal = 0;
    hashVal %= TABLE_SIZE;
    if (hashVal < 0){
        hashVal += TABLE_SIZE;
    }
    //1+(k mod m')
  return 1 + (hashVal % numeroPrimo);
}




//Calcolo numero primo per la giusta gestione del double hashing
int HashClass::getPrime(){
    
    for (int i =TABLE_SIZE-1;i>=1; i--) {
        int fact = 0;
        for (int j=2; j<= sqrt(i);j++ ) {
            if ( i%j ==0) {
                fact++;
            }if (fact==0) {
                return i;
            }
        }
    }
    return  3;
}

//Stampa il vocabolario
void HashClass::stampaVocabolario(){
    for (int i=0; i<TABLE_SIZE; i++) {
        if (hashTable[i]!=NULL) {
            std::cout<<hashTable[i]->getTermine()<<" : "<<hashTable[i]->getSignificato()<<"\n";
        }
    }

}


void HashClass:: ricercaConSuccesso(){
    float fattoreDiCarico=0.0F;
    float n =0.0f;
    //Calcolo gli slot occupati
    for(int i=0;i<TABLE_SIZE;i++){
        if (hashTable[i]!=NULL) {
            n++;
        }
    }
    fattoreDiCarico=(n/TABLE_SIZE);
    float mediRicerca = (1/fattoreDiCarico)*log(1/(1-fattoreDiCarico));
    std::cout<<"il tempo medio della ricerca con successo è: "<<mediRicerca<<" sec\n";
}



void HashClass::cercaAlternativa(HashItem *termine){
    int lcs;
    int  max =0;
    std::list <HashItem> parola;
    
    //In questo ciclo trovo tutte le parole con la max sottosequenza
    for (int i=0; i< TABLE_SIZE; i++) {
        if (this->hashTable[i]!=NULL) {
            lcs = lcsLength(termine->getTermine(), hashTable[i]->getTermine());
            //std::cout<<"\nLCS VALE:"<<lcs<<"\n";
            if (max<lcs) {
                max=lcs;
            }
        }
    }
    
    //Aggiungo tutte le parola con la max sotto sequenza in una lista
    for (int i=0; i<TABLE_SIZE; i++) {
        if (this->hashTable[i]!=NULL) {
            lcs = lcsLength(termine->getTermine(), hashTable[i]->getTermine());
            if (lcs == max) {
                parola.push_back(*hashTable[i]);
            }
            
        }
        
    }
    
    //Stampo tutti i possibili termi che l'utete forse cercava
    std::cout<<"\nForse cercavi:\n";
    std::list<HashItem>::iterator iterator =parola.begin();
    while (iterator!=parola.end() ){
        std::cout<<iterator->getTermine()<<" : "<<iterator->getSignificato()<<"\n";
        iterator++;
    }
}

// Restituisce la lunghezza dell LCS per le stringhe
int HashClass:: lcsLength(std::string X,std:: string Y)
{
    int m = int (X.length()+1);
    int n = int(Y.length()+1);
    int i, j;
    int L[m+1][n+1];
    for (i=0; i<=m; i++)
    {
        for (j=0; j<=n; j++)
        {
            //Per i =0 o j =0 lcs è 0
            if (i == 0 || j == 0){
                L[i][j] = 0;
            }
            //i due elementi sono uguali e quindi inserisco quell'elemento nell' lvs
            else if (X[i-1] == Y[j-1]){
                L[i][j] = L[i-1][j-1] + 1;
            }
            //I due elementi sono diversi
            else{
                L[i][j] = std::max(L[i-1][j], L[i][j-1]);
            }
        }
    }
    
    return L[m][n];
}


