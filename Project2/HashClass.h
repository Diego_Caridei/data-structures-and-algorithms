
#include <iostream>
#include <cmath>
#include <list>
#include "HashItem.h"
class HashClass {
private:
    //Tabella Hash
    HashItem **hashTable;
    int size;
    int numeroPrimo;
public:
     HashClass();
    ~HashClass();
    int getPrime();
    int getSize();
    HashItem* cerca(std::string termine);
    void rimuovi(std::string termine);
    void  riempi();
    void inserisci(std::string termine,std::string significato);
    void ricercaConSuccesso();
    void stampaVocabolario();
    //Calcola h1 k mod m
    int  hashMethodUno(std::string x);
    //Calcola h2 1+(k mod m')
    int  hashMethodDue(std::string x);
    //Calcola il valore hash (h1(k) + ih2(k)) mod m
    int  hashMethod(std:: string x);
    //LCS
    int lcsLength(std::string X,std:: string Y);
    void cercaAlternativa(HashItem *termine);
};