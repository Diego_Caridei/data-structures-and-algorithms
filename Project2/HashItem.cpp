#include "HashItem.h"
//Costruttore
HashItem:: HashItem (int hashVal, std::string termine, std::string significato){
    this->hashVal =hashVal;
    this->termine = termine;
    this->significato = significato;
}
//Distruttore
HashItem :: ~HashItem(){
    
}

//Setter  
void HashItem:: setTermine(std::string termine){
    this->termine=termine;
    
}
void HashItem:: setSignificato(std::string significato){
    this->significato = significato;
}

void HashItem::setHashVal(int hashVal){
    this->hashVal=hashVal;
}

//Getter
std::string HashItem:: getTermine(){
    return this->termine;
}
std::string HashItem:: getSignificato(){
    return this-> significato;
}

int HashItem:: getHashVal(){
    return this->hashVal;
}

