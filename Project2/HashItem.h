#include <iostream>
class HashItem{
private:
    int hashVal;
    std:: string termine;
    std:: string significato;
public:
    HashItem (int hashVal,std::string termine, std::string significato);
    ~HashItem();
    //setter
    void setTermine(std::string termine);
    void setSignificato(std::string significato);
    void setHashVal(int hashVal);
    //getter
    std::string getTermine();
    std::string getSignificato();
    int getHashVal();    
};